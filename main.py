#!/usr/bin/env python3
import sys
import time
from dotenv import dotenv_values
from selenium import webdriver
from selenium.common import WebDriverException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from webdriver_manager.chrome import ChromeDriverManager

COMPANY_OPTION_VALUE = "2"
DEFAULT_WAIT_FOR_TIMEOUT = 15

# Setup
config = dotenv_values("config.env")
username = config["LOGIN_USERNAME"]
password = config["LOGIN_PASSWORD"]

driverPath = ChromeDriverManager().install()
driverOption = Options()
driverOption.add_argument("--start-maximized")
driver = webdriver.Chrome(driverPath, options=driverOption)

# Login page
driver.get("http://hr.igloocg.com:8081/webtime")
driver.implicitly_wait(DEFAULT_WAIT_FOR_TIMEOUT)

inputUsername = driver.find_element(By.ID, "txtUser")
inputPassword = driver.find_element(By.ID, "txtPWD")
dropDownCompanySelection = Select(driver.find_element(By.ID, "DropDownList1"))
buttonLogin = driver.find_element(By.ID, "Button1")

inputUsername.click()
inputUsername.clear()
inputUsername.send_keys(username)

inputPassword.click()
inputPassword.clear()
inputPassword.send_keys(password)

dropDownCompanySelection.select_by_value(COMPANY_OPTION_VALUE)
buttonLogin.click()

# Check in/out page
driver.get("http://hr.igloocg.com:8081/webtime/Mobile/Work/WebAddInOut_m.aspx")
driver.implicitly_wait(DEFAULT_WAIT_FOR_TIMEOUT)

# Clean up after no windows left
while True:
    time.sleep(1)
    try:
        if (len(driver.window_handles) <= 0):
            driver.quit()
            break;
    except WebDriverException as err:
        print(err, file=sys.stderr, flush=True)
        driver.quit()
        break;
