# Automate Login
- login to tigersoft with web automation

# Getting started
- 1) Install dependencies with 'requirements.txt'
- 2) Edit 'config.env' with proper information
- 3) Launch 'main.py' (optional, run with 'run.bat')

# Dependencies
- python3
- python-dotenv
- selenium
- webdriver-manager
- chrome
